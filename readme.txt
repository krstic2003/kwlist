=== KWL WooCommerce Wishlist ===
Contributors: krstic2003
Tags: woocommerce, wishlist
Requires at least: 4.8
Tested up to: 4.9.8
Stable tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Simple Product Wishlist for WooCommerce.

== Description ==

Simple Product Wishlist for WooCommerce. Logged-in users and Guest can add WooCommerce Products in their list. Logged-in users can find Wishlist tab in MyAccount page. For Guests, there is a shortcode [kwlist] which displays wishlisted products.

= Docs & Support =

For install instruction and support you can check the [support forum](https://wordpress.org/support/plugin/kwlist/) on WordPress.org.


= Privacy Notices =

With the default configuration, this plugin, in itself, does not:

* track users by stealth;
* write any user personal data to the database;
* send any data to external servers;

With the default configuration, this plugin, in itself, use cookies.


= Requrired Plugins =

* WooCommerce

= Translations =

* Translations are currently not supported.

== Installation ==

1. Upload the entire 'kwlist' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Refresh permalniks by click on "Save Permalinks" button in Settings > Permalinks

You will find 'Woo Wishlist' menu in your WordPress admin panel. There you can adjust plugin options.

== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-5.png

== Changelog ==

= 1.0 =

* Initial Release


